package io.bit3.jsass;

public class JsassConstants {
  public static final String SASS_EXTENSION = ".sass";
  public static final String SCSS_EXTENSION = ".scss";
  public static final String CSS_EXTENSION = ".css";

  private JsassConstants() {
    throw new UnsupportedOperationException();
  }
}
