plugins {
    id("jsass.java-conventions")
    id("jsass.publication-conventions")
}

description = "The ultimate SASS compiler for Java / the JVM."

dependencies {
    implementation(libs.jetbrains.annotations)
}
